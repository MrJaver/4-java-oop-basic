package com.example.task04;


class Line {
    private Point pointOne, pointTwo;

    private void IsNull(Line l) {
        if (l == null) {
            throw new IllegalArgumentException("агрумент Line не может быть null");
        }
        Point.isNull(l.pointOne);
        Point.isNull(l.pointTwo);
    }

    public Point getPointOne() {
        return pointOne;
    }

    public Point getPointTwo() {
        return pointTwo;
    }

    public Line(Point p1, Point p2) {
        pointOne = p1;
        pointTwo = p2;
    }

    public String toString() {
        return String.format("(%s, %s)", pointOne.toString(), pointTwo.toString());
    }

    public void print() {
        System.out.println(toString());
    }

    public boolean isCollinearLine(Point p) {
        return (p.y * (pointTwo.x - pointOne.x) ==
                (pointTwo.y - pointOne.y) * p.x - pointOne.x * pointTwo.y + pointOne.x * pointOne.y + pointOne.y * (pointTwo.x - pointOne.x))
                && ((pointTwo.x <= p.x && p.x <= pointOne.x) || (pointOne.x <= p.x && p.x <= pointTwo.x))
                && ((pointTwo.y <= p.y && p.y <= pointOne.y) || (pointOne.y <= p.y && p.y <= pointTwo.y));
    }
}
