package com.example.task04;

/**
 * Класс точки на плоскости
 */
public class Point {
    final int x, y;

    public Point(int X, int Y) {
        x = X;
        y = Y;
    }

    static protected void isNull(Point point) {
        if (point == null) {
            throw new IllegalArgumentException("агрумент Point не может быть null");
        }
    }

    double distance(Point point) {
        isNull(point);
        return Math.sqrt(Math.pow(x - point.x, 2) + Math.pow(y - point.y, 2));
    }

    public String toString() {
        return String.format("(%d, %d)", x, y);
    }

    void print() {

        System.out.println(toString());
    }
}
