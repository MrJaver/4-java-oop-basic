package com.example.task03;

class ComplexNum {
    private double num;
    private double inum;

    public ComplexNum() {
    }

    ;

    private static void IsNull(ComplexNum cn) {
        if (cn == null) {
            throw new IllegalArgumentException("агрумент не может быть null");
        }
    }


    public ComplexNum(double num, double inum) {
        this.num = num;
        this.inum = inum;
    }

    public double getNum() {
        return num;
    }

    public void setNum(double num) {
        this.num = num;
    }

    public double getInum() {
        return inum;
    }

    public void setInum(double inum) {
        this.inum = inum;
    }

    static public ComplexNum add(ComplexNum base, ComplexNum adding) {
        IsNull(base);
        IsNull(adding);

        return new ComplexNum(base.num + adding.num, base.inum + adding.inum);
    }

    public ComplexNum add(ComplexNum adding) {
        IsNull(adding);

        return new ComplexNum(this.num + adding.num, this.inum + adding.inum);
    }

    static public ComplexNum multi(ComplexNum base, ComplexNum multiplier) {
        IsNull(base);
        IsNull(multiplier);

        return new ComplexNum(base.num * multiplier.num - base.inum * multiplier.inum,
                base.num * multiplier.inum + base.inum * multiplier.num);
    }

    public ComplexNum multi(ComplexNum multiplier) {
        IsNull(multiplier);

        return new ComplexNum(this.num * multiplier.num - this.inum * multiplier.inum,
                this.num * multiplier.inum + this.inum * multiplier.num);
    }

    public String toString() {
        return String.format("(%f, i*%f)", num, inum);
    }

    public void print() {
        System.out.println(toString());
    }

    ;
}
