package com.example.task03;

public class Task03Main {
    public static void main(String[] args) {

        ComplexNum first = new ComplexNum(23,32),
                second = new ComplexNum(14,41),
                third = ComplexNum.add(first,second),
                four = ComplexNum.multi(first,second);

        System.out.print(String.format("%s + %s = %s",first.toString(),second.toString(),third.toString()));

        System.out.print(String.format("%s * %s = %s",first.toString(),second.toString(),four.toString()));
    }
}
