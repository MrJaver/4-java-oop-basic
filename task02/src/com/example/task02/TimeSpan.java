package com.example.task02;

class TimeSpan {
    private int hour;
    private int minute;
    private int second;

    private void correct() {
        minute = (minute + second / 60);
        hour = (hour + minute / 60);
        minute %= 60;
        second %= 60;
    }

    private void isNull(TimeSpan ts) {
        if (ts == null)
            throw new IllegalArgumentException("агрумент не может быть null");
    }


    public TimeSpan() {
    }

    ;

    public TimeSpan(int H, int M, int S) {
        hour = H;
        minute = M;
        second = S;

        correct();
    }

    void add(TimeSpan time) {
        isNull(time);

        hour += time.hour;
        minute += time.minute;
        second += time.second;

        correct();
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    void subtract(TimeSpan time) {
        isNull(time);
        hour -= time.hour;
        minute -= time.minute;
        second -= time.second;
        correct();
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String toString() {

        return String.format("(%d, %d, %d)", hour, minute, second);
    }

}
