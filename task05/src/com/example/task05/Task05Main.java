package com.example.task05;

import java.util.Arrays;
import java.util.stream.Stream;
public class Task05Main {

    public static void main(String[] args) {
        Point[] A={new Point(12,12)};
        PolygonalLine L=new PolygonalLine(A);
        L.addPoint(12,23);
        System.out.println(L.getLength());
    }
}
