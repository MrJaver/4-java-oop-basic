package com.example.task05;

import java.util.Arrays;

/**
 * Ломаная линия
 */
public class PolygonalLine {

    private Point[] points;

    static private void isNull(Point[] points) {
        if (points == null)
            throw new IllegalArgumentException("агрумент Point[] не может быть null");
    }

    public PolygonalLine() {
    }

    ;

    public PolygonalLine(Point[] points) {
        isNull(points);

        this.points = points;
    }

    public void setPoints(Point[] points) {
        isNull(points);

        this.points = new Point[points.length];
        for (int i = 0; i < points.length; i++) {
            Point.isNull(points[i]);

            this.points[i] = new Point(points[i].getX(), points[i].getY());
        }
    }

    public void addPoint(Point point) {
        Point.isNull(point);

        if (points == null || points.length == 0) {
            points = new Point[1];
            points[0] = new Point(point.getX(), point.getY());
        }
        points = Arrays.copyOf(points, points.length + 1);
        points[points.length - 1] = new Point(point.getX(), point.getY());
    }

    public void addPoint(double x, double y) {
        if (points == null || points.length == 0) {
            points = new Point[1];
            points[0] = new Point(x, y);
            return;
        }
        points = Arrays.copyOf(points, points.length + 1);
        points[points.length - 1] = new Point(x, y);
    }


    public double getLength() {
        isNull(this.points);
        double sum = 0;
        for (int i = 0; i < points.length - 1; i++) {
            Point.isNull(points[i]);
            Point.isNull(points[i + 1]);

            sum += points[i].getLength(points[i + 1]);
        }
        return sum;
    }

}
