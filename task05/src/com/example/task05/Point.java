package com.example.task05;

/**
 * Точка в двумерном пространстве
 */
public class Point {

    private double x, y;

    public Point(double X, double Y) {
        x = X;
        y = Y;
    }

    protected static void isNull(Point point) {
        if (point == null) {
            throw new IllegalArgumentException("агрумент Point не может быть null");
        }
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getLength(Point point) {
        isNull(point);

        return Math.sqrt(Math.pow(point.x - x, 2) + Math.pow(point.y - y, 2));
    }

}
