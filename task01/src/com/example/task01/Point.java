package com.example.task01;

/**
 * Класс точки на плоскости
 */
public class Point {
    int x;
    int y;

    private void isNull(Point point) {
        if (point == null) {
            throw new IllegalArgumentException("агрумент не может быть null");
        }
    }


    public Point() {
    }

    ;

    public Point(int X, int Y) {
        x = X;
        y = Y;
    }

    void flip() {
        int copy = x;
        x = -y;
        y = -copy;

    }

    double distance(Point point) {
        isNull(point);
        return Math.sqrt(Math.pow(x - point.x, 2) + Math.pow(y - point.y, 2));
    }

    public String toString() {
        return String.format("(%d, %d)", x, y);
    }

    void print() {

        System.out.println(toString());
    }
}
